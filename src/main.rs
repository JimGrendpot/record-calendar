#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate rocket;
extern crate rocket_contrib;
use rocket::http::RawStr;
use std::path::{Path, PathBuf};

use rocket::config::{Config, Environment};
use rocket::request::Form;
use rocket::response::{Flash, Redirect};
use rocket_contrib::serve::StaticFiles;
use rocket_contrib::templates::Template;

extern crate time;
use std::env;
use time::Weekday::*;
use time::{Date, Duration, Time};

extern crate calmsch2;
use calmsch2::w_sql;

use postgres::row::Row;

use rocket::fairing::AdHoc;
use rocket::response::NamedFile;
use rocket::State;

struct AssetsDir(String);

#[get("/<asset..>")]
fn assets(asset: PathBuf, assets_dir: State<AssetsDir>) -> Option<NamedFile> {
    NamedFile::open(Path::new(&assets_dir.0).join(asset)).ok()
}

/// Структуры делятся на два типа - из формы и на сайт
/// На сайт  - #[derive(Serialize)]
/// Из формы - #[derive(FromForm)]
#[derive(FromForm)]
struct Doctors {
    date: String,
    namecal3: String,
}

#[derive(FromForm)]
struct Remove {
    id: i32,
    date: String,
}

#[derive(Serialize)]
struct Client {
    id: i32,
    name: String,
}

#[derive(Serialize)]
struct Names {
    calendar: Vec<Vec<i32>>,
    date: String,
    clie: Vec<Client>,
}

#[derive(Serialize)]
struct RecordCal {
    id: i32,
    name: String,
}

#[derive(FromForm)]
struct AddWeekRecord {
    np: i32,
    name: String,
    date: String,
}

#[derive(Serialize)]
struct Week {
    calendar: Vec<Vec<RecordCal>>,
    dates: Vec<String>,
    week_n: u8,
    week_times: Vec<String>,
    year_n: i32,
}
///Удаление записи
#[post("/del_gvs", data = "<content>")]
fn del_poc(content: Form<Remove>) -> Flash<Redirect> {
    let mut conn = w_sql::connect_db();
    w_sql::remove_pac(&mut conn, &content.id);
    Flash::success(
        Redirect::to(format!("/opendate/{}", content.date)),
        "Successfully.",
    )
}

///Удаление записи в недели
#[post("/del_gvs2", data = "<content>")]
fn del_record_week(content: Form<Remove>) -> Flash<Redirect> {
    let mut conn = w_sql::connect_db();
    let mut nweek = Date::parse(&content.date, "%F")
        .unwrap()
        .format("%W")
        .parse::<i32>()
        .unwrap();

    let year = Date::parse(&content.date, "%F")
        .unwrap()
        .format("%Y")
        .parse::<i32>()
        .unwrap();
    nweek += 1;
    w_sql::remove_pac(&mut conn, &content.id);
    Flash::success(
        Redirect::to(format!(
            "/openweek/{}/{}",
            year.to_string(),
            nweek.to_string()
        )),
        "Successfully.",
    )
}

///Добавление записи в неделю
#[post("/gvs1", data = "<content>")]
fn add_record_week(content: Form<AddWeekRecord>) -> Flash<Redirect> {
    if !content.name.is_empty() {
        let mut conn = w_sql::connect_db();

        let mut nweek = Date::parse(&content.date, "%F")
            .unwrap()
            .format("%W")
            .parse::<i32>()
            .unwrap();
        let year = Date::parse(&content.date, "%F")
            .unwrap()
            .format("%Y")
            .parse::<i32>()
            .unwrap();

        nweek += 1;
        println!("{}", nweek);
        w_sql::add_record_week(&mut conn, &content.name, &content.date, &content.np);
        Flash::success(
            Redirect::to(format!(
                "/openweek/{}/{}",
                year.to_string(),
                nweek.to_string()
            )),
            "Successfully.",
        )
    } else {
        Flash::success(Redirect::to("/"), "Error")
    }
}

///Добавление записи в день
#[post("/gvs", data = "<content>")]
fn add_poc(content: Form<Doctors>) -> Flash<Redirect> {
    println!("aaaaaa1");
    if (!content.date.is_empty()) && !content.namecal3.is_empty() {
        let mut conn = w_sql::connect_db();
        w_sql::add_doctor(&mut conn, &content.namecal3, &content.date);
        Flash::success(
            Redirect::to(format!("/opendate/{}", content.date)),
            "Successfully.",
        )
    } else {
        Flash::success(Redirect::to("/"), "Error")
    }
}

///Страница недели
#[get("/openweek/<year>/<week>")]
pub fn week(week: u8, year: i32) -> Template {
    let mut conn = w_sql::connect_db();

    let mut week_ob = Week {
        calendar: Vec::new(),
        dates: Vec::new(),
        week_n: week,
        week_times: Vec::new(),
        year_n: year,
    };

    let first_day = Date::try_from_iso_ywd(year, week, Monday).unwrap();
    let last_day = Date::try_from_iso_ywd(year, week, Sunday).unwrap();
    println!("firstday {} lastday {}", &first_day, &last_day);

    let first_day_n = first_day.julian_day();
    let last_day_n = last_day.next_day().julian_day();
    let kol_records = 10;
    let duration = 6_i64;

    let mut times = Vec::new();
    times.push(Time::try_from_hms(8_u8, 0_u8, 0_u8).unwrap());
    for i in 0..kol_records {
        times.push(times[i] + Duration::minutes(duration));
        print!("{}", times[i]);
    }

    for i in times {
        week_ob.week_times.push(i.format("%H:%M"));
    }

    println!("");
    let mut a = 0;
    for i in first_day_n..last_day_n {
        week_ob.calendar.push(Vec::new());

        let i_str = Date::from_julian_day(i as i64);
        week_ob.dates.push(i_str.to_string());

        for j in 0..10 {
            week_ob.calendar[a].push(RecordCal {
                id: j as i32,
                name: "".to_string(),
            });
        }
        a += 1;
    }

    let aas: Vec<Row> =
        w_sql::get_list_pr_week(&mut conn, &first_day.to_string(), &last_day.to_string());

    for row in aas {
        let id = row.get::<_, i32>(0);
        let name = row.get::<_, String>(1);
        let date = row.get::<_, Date>(2);
        //let time = row.get::<_, Time>(3);
        let num_time = row.get::<_, i32>(4);
        if num_time != -1 {
            let mut a = 0_usize;
            for j in first_day_n..(last_day_n + 1) {
                let mut b = 0_usize;
                for i in 0..10 {
                    //           println!("{}", Date::from_julian_day(j));
                    if date.julian_day() == j && num_time == i {
                        week_ob.calendar[a][b].id = id;
                        week_ob.calendar[a][b].name = name.to_string();
                    }
                    b += 1;
                }
                a += 1;
            }
        }
    }

    for _j in 0..10 {
        let mut a = 0;
        for _i in first_day_n..last_day_n {
            a += 1;
        }
    }

    Template::render("week", &week_ob)
}

///Открытие даты
#[get("/opendate/<date>")]
pub fn date(date: &RawStr) -> Template {
    let mut conn = w_sql::connect_db();
    let aas: Vec<Row> = w_sql::get_list_pr(&mut conn, &date.to_string());

    let mut ret_names = Names {
        calendar: Vec::new(),
        date: date.to_string(),
        clie: Vec::new(),
    };

    for row in aas {
        ret_names.clie.push(Client {
            id: row.get::<_, i32>(0),
            name: row.get::<_, String>(1),
        });
    }
    Template::render("index", &ret_names)
}

///Форма даты
#[derive(Serialize)]
struct Dates {
    title: String,
    urls: Vec<String>,
}

///Главная страница
#[get("/")]
pub fn root_r() -> Template {
    let date = Date::today();
    let year = date.year();
    let week = date.week();

    let mut dates = Dates {
        title: "Главная".to_string(),
        urls: Vec::new(),
    };

    dates.urls.push(format!("/openweek/{}/{}", year, week));

    Template::render("i", &dates)
}

///Главная функция
fn main() {
    let mut static_f = "./static";

    let r_env = env::var("ROCKET_ENV").unwrap_or("dev".to_string());
    let mut config = Config::new(Environment::Production);

    if r_env == "prod" {
        static_f = "/var/site2/static/";
        config.set_port(80);

        print!("{}", env::var("ROCKET_ENV").unwrap());
    }

    rocket::ignite()
        .mount(
            "/",
            routes![
                root_r,
                date,
                week,
                del_poc,
                add_poc,
                del_record_week,
                add_record_week
            ],
        )
        .mount("/static", StaticFiles::from(static_f))
        .attach(Template::fairing())
        .attach(AdHoc::on_attach("Assets Config", |rocket| {
            let assets_dir = rocket
                .config()
                .get_str("assets_dir")
                .unwrap_or("/var/site2/assets/")
                .to_string();

            Ok(rocket.manage(AssetsDir(assets_dir)))
        }))
        .launch();
}
