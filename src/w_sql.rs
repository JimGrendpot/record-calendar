extern crate postgres;
use postgres::row::Row;
use postgres::{Client, NoTls};
use time::Date;

pub fn connect_db() -> Client {
    Client::connect("postgres://site2:PEZD4@localhost/site2", NoTls).unwrap()
}
/// Создание базы

pub fn init_db(client: &mut Client) {
    client
        .batch_execute(
            "
        CREATE TABLE ticks(
            id          SERIAL PRIMARY KEY, 
            date_tik    DATE NOT NULL,
            time_tik    TIME,
            names       TEXT,
            active      BOOL DEFAULT TRUE
        );
        CREATE TABLE params(
            id          SERIAL PRIMARY KEY,
            name        TEXT NOT NULL,
            params      TEXT
        );",
        )
        .unwrap();
}

pub fn kill_db(client: &mut Client) {
    client.batch_execute("DROP TABLE schedule;").unwrap();
}

#[test]
fn test_init_kill_db() {
    let mut con = connect_db();
    init_db(&mut con);
    kill_db(&mut con);
}

/// Добавление записи в недели
/// # Example
/// ```
/// use use calmsch2::w_sql;
///
/// let mut conn = w_sql::connect_db();
/// add_record_week(&mut conn, &"ФИО".to_string(), &"23.09.2020", 3);
/// ```
pub fn add_record_week(client: &mut Client, name: &String, date: &String, nt: &i32) {
    let date1 = Date::parse(date, "%F").unwrap();
    client
        .execute(
            "INSERT INTO ticks (names, date_tik, time_tik, nt)
                    VALUES ($1, $2, now(), $3);",
            &[name, &date1, &nt],
        )
        .unwrap();
}

/// Функция, аналогичная add_record_week
/// Только в ней дата в таблицу отправляеся не как поле типа String,
/// а как поле Date
/// # Example
/// ```
/// use use calmsch2::w_sql;
///
/// let mut conn = w_sql::connect_db();
/// add_doctor(&mut conn, "ФИО".to_string(), "23.09.2020".to_string());
///  ```
pub fn add_doctor(client: &mut Client, name: &String, date: &String) {
    let date1 = Date::parse(date, "%F").unwrap();
    client
        .execute(
            "INSERT INTO ticks (names, date_tik, time_tik)
                    VALUES ($1, $2, now());",
            &[name, &date1],
        )
        .unwrap();
}

/// Удаление записи.
/// На вход подаётся id записи.
/// # Example
/// ```
/// use use calmsch2::w_sql;
///
/// let mut conn = w_sql::connect_db();
/// remove_pac(&mut conn, 3);
/// ```
pub fn remove_pac(client: &mut Client, id: &i32) {
    client
        .execute(
            "UPDATE ticks
             SET active = false
             WHERE id = $1;",
            &[id],
        )
        .unwrap();
}
/// Получание конфигураций
pub fn get_week_config(client: &mut Client, week: i32, year: i32) -> Vec<Row> {
    client
        .query(
            "SELECT kol_record, interval 
               FROM week
               WHERE week_n == $1 and year = %2;",
            &[&week, &year],
        )
        .unwrap()
}

/// получение списка записей в неделю
/// # Example
/// ```
/// use use calmsch2::w_sql;
///
/// let mut conn = w_sql::connect_db();
/// let a = get_list_pr_week(&mut conn, "21.09.2020".to_string(),"23.09.2020".to_string());
/// let id       = a[0].get::<_, i32>(0);
/// let name     = a[0].get::<_, String>(1);
/// let date     = a[0].get::<_, Date>(2);
/// let time     = a[0].get::<_, time>(3);
/// let num_time = a[0].get::<_, i32>(4);
/// ```
pub fn get_list_pr_week(client: &mut Client, first_day: &String, last_day: &String) -> Vec<Row> {
    let date1 = Date::parse(first_day, "%F").unwrap();
    let date2 = Date::parse(last_day, "%F").unwrap();

    print!("{}", date1);
    client
        .query(
            "SELECT id, names, date_tik, time_tik, nt
                  FROM ticks
                  WHERE date_tik <= $2 AND date_tik >= $1 AND active = true;",
            &[&date1, &date2],
        )
        .unwrap()
}

/// Получение записей на день
pub fn get_list_pr(client: &mut Client, date: &String) -> Vec<Row> {
    let date1 = Date::parse(date, "%F").unwrap();
    client
        .query(
            "SELECT id, names
                  FROM ticks
                  WHERE date_tik = $1 AND active = true;",
            &[&date1],
        )
        .unwrap()
}
